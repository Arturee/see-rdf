*This project is public only for presentation purposes. You have no permission to use, modify, or share the software.*

### Technologies ###

Java, Javafx, (Ant)

### Introduction ###

This is an RDF data visualizer. Data is loaded from a file and the result can be saved as SVG.

![demo](readme/rdf.gif)

### Technical ###

Most RDF serialization types are supported as input. A force-directed layout algorithm
is used to achieve a balanced graph layout. Nodes and edges are implemented via JavaJx
labels and polygons.

### Libs ###

Apache Jena *(for loading RDF, version 3.6 is OK)*, Apache Batik *(for drawing SVG, version 1.7 is OK)*


### Deployment ###

Compiled via Ant *(build.xml script)*.
Works out of the box on any machine with Java (and JavaFx). Tested on Windows an Linux.