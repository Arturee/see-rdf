package application;

import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class DraggableLabel extends Label {	
	private Coordinate mouseOld;
	private boolean dragging;
	
	public DraggableLabel(VisualNode n){
		mouseOld = new Coordinate(0.0, 0.0);
		dragging = false;
		setListeners(n);
	}
	
	
	private void setListeners(VisualNode n){
		this.setOnMousePressed((MouseEvent e)->{
			if(e.isPrimaryButtonDown()){
				setMouseOld(e);
			} else if (e.isSecondaryButtonDown()){
				ColorDialogController.show(n);
			}			
		});
		this.setOnMouseDragged((MouseEvent e)->{
			if (e.isPrimaryButtonDown()){
				move(e);
				setMouseOld(e);
				dragging = true;
			}
			
		});
		this.setOnMouseReleased((MouseEvent e)->{
			if (e.isPrimaryButtonDown()){
				if (dragging){
					move(e);
					dragging = false;
				}
			}
		});
		
		
	}
	
	private void setMouseOld(MouseEvent e){
		mouseOld.x = e.getSceneX();
		mouseOld.y = e.getSceneY();
	}
	private void move(MouseEvent e){
		Coordinate change = new Coordinate(e.getSceneX() - mouseOld.x, e.getSceneY() - mouseOld.y);
		this.relocate(this.getLayoutX() + change.x, this.getLayoutY() + change.y);
	}
	
	@Override
	public void relocate(double x, double y){
		super.relocate(Math.max(0, x), Math.max(0, y));
	}
	
	
	

}
