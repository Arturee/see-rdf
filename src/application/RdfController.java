package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;

import javafx.scene.layout.Pane;

public class RdfController {

	//TODO otestovat na anonymnich uzlech
	
	public static void loadRdfIntoCollections(String filename, Collection<VisualNode> nodes, Collection<VisualEdgeDirected> edges, Pane parent){
		//TODO naloadovat javadoc a zjistit, jestli nehrozi dalsi vyjimky
		//TODO assert not null
		//TODO cancel loading if filesize > 200KB eg. (or if there are too many nodes)
		
		//format RDF souboru knihovna zjisti z koncovky
		try (InputStream in = new FileInputStream(filename)){
			HashMap<String, VisualNode> imported = new HashMap<>();
			
			Model m = RDFDataMgr.loadModel(filename); //haze org.apache.jena.riot.RiotException
			ResIterator subjects = m.listSubjects();
			
			while (subjects.hasNext()){
				Resource s = subjects.next();
				String sUri = s.toString();
				VisualNode source = imported.get(sUri); //muze byt null
				if (source == null){
					source = new VisualNode(sUri , parent);
				}
				
				for(StmtIterator outEdges = s.listProperties(); outEdges.hasNext();){
					Statement stmt = outEdges.next();
					Property p = stmt.getPredicate();
					RDFNode o = stmt.getObject();
					
					String oUriOrValue = o.toString(); //MEBBE vytvorit specialni uzel pro kazdeou value (multiplikovat kdyz se nejedna o URI)
					VisualNode target = imported.get(oUriOrValue);
					if (target == null){
						target = new VisualNode(oUriOrValue, parent);
					}
					
					VisualEdgeDirected edge = new VisualEdgeDirected(p.toString(), source, target, parent);
					source.addEdgeOut(edge);
					target.addEdgeIn(edge);
					
					imported.put(sUri, source);
					imported.put(oUriOrValue, target);
					
					nodes.add(source);
					nodes.add(target);
					edges.add(edge);
				}
			}
		} catch(org.apache.jena.riot.RiotException e){
			e.printStackTrace();
			Err.showAlert("Your RDF file probably contains some errors in format.");
		} catch (FileNotFoundException e){
			e.printStackTrace();
			Err.showAlert("File could not be found. Please make sure you have selected an existing file and try again.");
		} catch (IOException e){
			e.printStackTrace();
			Err.showAlert("An error occured after reading the file. Your graph may not be loaded correctly");
			//autoclosable .close() exception - TODO how did it occur anyway?
		}
	}
	

	
}
