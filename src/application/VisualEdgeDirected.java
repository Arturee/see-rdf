package application;

import java.awt.Graphics2D;
import java.io.Serializable;

import javafx.scene.layout.Pane;


public class VisualEdgeDirected implements Serializable {
	private static final long serialVersionUID = 1L; //ser
	
	private VisualNode source;
	private VisualNode target;
	private transient Arrow arrow;
	private String uri;
	
	//ser
	private String serText;
	
	public VisualEdgeDirected(){} //ser
	public VisualEdgeDirected(String uri, VisualNode source, VisualNode target, Pane parent){
		this.uri = uri;
		this.source = source;
		this.target = target;
		arrow = getArrow(parent);
	}
	
	public void prepareForSerialization(){ //ser
		serText = arrow.getText();
	}
	public void finishDeserialization(Pane parent){ //ser
		arrow = getArrow(parent);
		arrow.setText(serText);
		refresh();
	}
	
	
	private Arrow getArrow(Pane parent){
		if (isCycle()){
			arrow = new CyclicArrow(uri, parent, source.getLabel());
		} else {
			arrow = new StraightArrow(uri, parent, source.getCenter(), target.getCenter(), target.getLabel());
		}
		return arrow;
	}
	private boolean isCycle(){
		return source == target;
	}
	
	public void refresh(){
		arrow.refreshOnGui(source.getCenter(), target.getCenter(), target.getLabel());
	}
	public String getUri(){
		return uri;
	}
	//MEBBE merge functions
	public void showShortened(String shortened){
		arrow.setText(shortened);
	} 
	public void showFullUri(){
		arrow.setText(uri);
	}
	public void paint(Graphics2D g){
		arrow.paint(g);
	}
	public VisualNode getSource(){
		return source;
	}
	public VisualNode getTarget(){
		return target;
	}


}
