package application;

public class Coordinate {
	public double x,y;
	public Coordinate(double x, double y){
		this.x =x;
		this.y = y;
	}
	public Coordinate(Coordinate c){
		this.x = c.x;
		this.y = c.y;
	}
	
	public void add(Vector v){
		x += v.x;
		y += v.y;
	}

}



