package application;



public class Informative {
	
		
		
	/**
	 * TT: 58	(including 19h studying libraries, preparing spec. and building)
	 *
	 * TODO do paint() pridat style (dashed, dotted, atd), poresit .svg velke soubory (spodek se orizne-proc?)
	 * TODO co kdyz se zmackne save hned po close?
	 * TODO pri de/serializaci brat v potaz i show-options
	 * 
	 * TODO pridat shorcuts
	 * TODO poresit VSECHNY vyjimky
	 * TODO random (vertical) position correction (also for edge-labels)
	 * TODO celestial scramble
	 * TODO rdf schemas
	 * TODO full code review + try to attack the app (and debug)
	 * 
	 * TODO udelat interface pro serializaci = better code
	 * TODO debug apache jena = zda se, ze pokud v rdf/xml jsou prazdne radky (specielne pred xml-prefacem) tak vznikne exception
	 * TODO pouzit jen ty casti knihoven, kt. jsou potreba? (je to technicky mozne? dovoluje to licence?)
	 * TODO poresit licence
	 *
	 * MEBBE deserialize zaridit aby pouzivalo list misto hashmap (predelat Collection na List)
	 * MEBBE opravit zoom (a aby nezalizal pod minComputedHeight a Width)
	 * MEBBE multiple (rectangular) node-select and move (edit color) 
	 * MEBBE zaridit aby labely byly vertikalne tahaci (horizontal se opravi sam)
	 * MEBBE zbavit se log4j chyb - viz web
	 * MEBBE zpresnit pozici ulkadani textu
	 * MEBBE dat ukladani do non-gui vlakna (problem je cekani cca 4sec., horsi vstupy pak umrou na out-of-memory)
	 * MEBBE zachytavat non-fatal exceptions?
	 * MEBBE load configuration from last run? (path to last loaded file for file selection)
	 * 
	 * -----------------------------------
	 * DOKUMENTACE:
	 * 
	 * java version 1.8.0_66
	 * 
	 * - seerdf zobrazuje jen jeden graf (nepojmenovany)
	 * - open sche. funguje jen, kdyz jsou v rdf/xml syntaxu
	 * - pokud uzivatel nezada file-extension pri ukaldani obrazku, automaticky se prida .png (chovani javafx)
	 * 
	 * - pro cca 2K hran je ukladani okamzite a randomize prakticky okamzite
	 * - to same pri rozliseni 5.5Kx5.5K zvladne asi za 4 sec. (vetsi zatez uz vetsinou vyvola out-of-memory exception)
	 * 
	 * 
	 * 
	 * 
	 * 
	 *************************************************
	 *
	 * LEGENDA = POZOR | TODO | MEBBE
	 * 
	 * -----------------------------------
	 * CREDIT:
	 * 
	 * Apache Jena - Apache License, Version 2.0
	 * Apache Batik - Apache License, Version 2.0
	 * 
	 * -----------------------------------
	 * VIZ:
	 * 
	 * http://stackoverflow.com
	 * https://docs.oracle.com/javafx/
	 * https://docs.oracle.com/javase/8/javafx/
	 * https://jena.apache.org/tutorials/
	 * https://jena.apache.org/documentation/rdf/
	 * https://jena.apache.org/documentation/ontology/
	 *  
	 *************************************************
	 * IMPEMENTACNI HNUSY - POZOR
	 * 
	 *  POZOR knihovny se navzajem perou - muze za to Xerses (viz https://jena.apache.org/documentation/query/faq.html)
	 *  POZOR eclipse generuje .gitignore, kt. schova vse v /bin (takze ukladat .fxml do /bin nepripada v uvahu)
	 *  POZOR javafx.scene.paint.Color neni Serializable bohuzel.
	 *  POZOR ve scenebuilderovi je potreba nastavit spravny controller a to plnym jemenm - eg. application.MainFormController
	 *  POZOR aby v dialogu fungoval cerveny krizek, je potrega pridat button type typu ButtonType.CANCEL_CLOSE
	 * 	POZOR javafx.eventy se nedavaji do event queue, protoze kdyz se v handleru fireuje event, tak se nejdrive provede jeho handler a pak
	 * 		se dokonci puvodni.
	 *  POZOR kdyz listener zmeni GUI, tak zmeny, ktere nejsou explicitne v kodu, ale dopocitaji je automaticky, se spocitaji az
	 *  	po skonceni listeneru.
	 *  POZOR zahadny problem s ImageIO.write ... BufferedImage.TYPE_INT_ARGB pro BufferedImage uplne prehazi barvy (skoro se zda ze je invertuje)
	 *  	reseni je pouzit BufferedImage.TYPE_INT_RGB.
	 *  POZOR javafx controls nemaji metodu paint, coz je problem pri vykreslovani GUI do .jpg, .svg atd.
	 * 	POZOR javafx nepodporuje z-index (ten se sam urci - to co bylo pridane nejpozdeji ma nejvyssi)
	 * 	POZOR s listView a tableView se strasne spatne pracuje (insert dat, zmena dat atd.)
	 * 	POZOR vsechen GUI kod bezi v Javafx Application Threadu - a zadne jine vlakno nemuze pracovat s GUI (javafx scene graph), jelikoz
	 * 	GUI neni thread safe.
	 * 
	 *	POZOR, scene builder casto kdyz uklada scenu tak necommitne do MainForm (asi to zustane v cachi) a pak novy layout nefunguje
	 *  POZOR ve formControlleru (kt. ma Initializable interface) v metode initialize() jeste nejsou nastavene dulezite fieldy gui
	 *  	prvku - napr getHeight vraci 0!
	 *	Kdyz actionListener vyhodi vyjimku, tak se zabije jeho vlakno (a vypise se hlaska do konzole), ale aplikace bezi dal.
	 *	Potomci ScrollPane si nemohou volit polohu.
	 *	Obecne v javafx lze vsechno delat 10-ti zpusoby a take kazdy gui node se dost lisi a to i v metodach|fieldech, kt. by clovek
	 *		cekal, ze budou definovane u vsech nodu - dusledkem je, ze je potreba cist dokumentaci pro kazdy node znovu.
	 *	slider.valueChangingProperty spatne reaguje - resenim je slider.valeProperty, kt. ma udajne stejnou funkci, ale reaguje dobre.
	 *	Pro ScrollPane neni mozne oddelit od sebe mouseWheel a scrollery - defaultni handler scrolluje pri mousewheelu a nelze zmenit.
	 * 	Width a Height jsou vzdy double!
	 *	POZOR parent Pane neni jeho opravdovy parent ScrollPane, ale nejaky ScrollPaneSkin, a jeho parent je dokonce takze dalsi SPSkin.
	 * 
	 * 
	 **************************************************
	 * LEARNED
	 * 	javafx with dialogs
	 * 	draggable nodes via labels
	 *	wraps
	 *	java serializable + transient
	 *	lambdas, event driven programming
	 *	svg and raster output
	 *	git
	 *	using static keyword frequently
	 *	protected
	 *	copy constructor instead of .clone()
	 *  @Deprecated, @Override, final!
	 * 	FX scene builder
	 * 	using external .jars
	 *	graphics2D
	 *	using instances of inner classes
	 *	eclipse IDE - organize imports, select variables by double clicking, find+replace (local/global), tasks - zobrazi TODOcka
	 * 
	 ***************************************************
	 * 
	 * DEPLOYMENT
	 * 	je potreba napsat si vlastni ant script (eclipse defaultne smaze *.fxml a efxclipse je spatne zdokumentovane)
	 * 
	 */
	
}
