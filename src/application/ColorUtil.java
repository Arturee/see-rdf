package application;

public abstract class ColorUtil {
	
	/*
	 * awt
	 * 	RGBA - int 0..255
	 * 
	 * javafx
	 * 	RGBA - double 0.0..0.1
	 * 
	 * 
	 * 
	 * */
	
	
	
	public static java.awt.Color fxToAwt(javafx.scene.paint.Color c){
		return new java.awt.Color((float)c.getRed(), (float)c.getGreen(), (float)c.getBlue(), (float)c.getOpacity());
	}
	public static String fxToWeb(javafx.scene.paint.Color c){
		//nepocita s alphou
		String R = fxRangeTo2hexRange(c.getRed());
		String G = fxRangeTo2hexRange(c.getGreen());
		String B = fxRangeTo2hexRange(c.getBlue());
		return "#"+R+G+B;	
	}
	private static String fxRangeTo2hexRange(double d){
		//d in [0.0, 1.0] //TODO assert
		final int MAX = 255;
		return String.format("%02X", (int)(MAX*d));
	}

	
	
}
