package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;





//nastavi se jako controller MainForm.fxml a FXMLLoader si ho pak nacte kdyz si nacita GUI

public class MainFormController implements Initializable {
	@FXML private Button buttonScrambleEggs;
	@FXML private Button buttonReset;
	@FXML private Button zin;
	@FXML private Button zout;
	@FXML private Slider sliderZoom; //0-100
		
	@FXML private MenuItem menuFileOpen;
	@FXML private MenuItem menuFileSaveAs;
	@FXML private MenuItem menuFileClose;
	@FXML private MenuItem menuFileExit;
	
	@FXML private CheckMenuItem menuShowShortEdgeLabels;
	@FXML private CheckMenuItem menuShowShortNodeLabels;
	@FXML private CheckMenuItem menuShowEdgeLabels;
	@FXML private CheckMenuItem menuShowNodeLabels;
	@FXML private CheckMenuItem menuShowFullEdgeLabels;
	@FXML private CheckMenuItem menuShowFullNodeLabels;

	@FXML private AnchorPane playground;

	@FXML private SplitPane splitPaneOuter; 
	@FXML private TabPane tabPaneRight;
	@FXML private ListView<String> listViewShortcuts;
	


	//logika
	private VisualGraph graf;


	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) { //zavola se potom, co se vytvori GUIstrom
		//POZOR zde jeste neni GUI neni zobrazene (gui.show()), takze velikosti a dalsi properties NEJSOU nastavene/spocitane
	
		menuShowEdgeLabels.setSelected(true);
		menuShowNodeLabels.setSelected(true);
		
		
		playground.setStyle("-fx-border-color: #d8dbeb; -fx-border-style: dashed; -fx-border-width: 3px");
		playground.setPrefSize(600, 600);
		playground.setMinSize(500, 500);
		/*	z nejakeho duvodu nejde zvolit mensi ... asi protoze playground obsahuje nejake objekty
		 *	(rdf graf), tak jeho computed size je vzdy alespon cca 450x450 
		 * */
	
		SplitPane.setResizableWithParent(tabPaneRight, false);
		sliderZoom.setValue(50.0);
		
		listViewShortcuts.setItems(Shortcut.getShortcutsForGUI());
	}

	
	
	
	private void zoom(double z){
		double height = playground.getPrefHeight();
		double width = playground.getPrefWidth();
		
		double minw = playground.getMinWidth();
		double minh = playground.getMinHeight();

		
		double h = Math.max(height * z, minh);
		double w = Math.max(width * z, minw);
		
		playground.setPrefSize(h, w);
	}
	
	
	
	
	
	
	
	private double getMag(double val){ //mezi -100 a 100
		//prevede 0-100 na 1/5.0 az 5.0 rovnomerne

		final double DIV_FACTOR = 20.0;
		final double MUL_FACTOR = 5/600.0;
		final double CORRECTION = 0.95;
		
		if (val < 0.0){
			val = Math.abs(val * MUL_FACTOR);
			return (1 - val) * CORRECTION;	//1 az 1/6
		} else if (val > 0.0){
			return 1 + val / DIV_FACTOR;	//1 az 6
		} else {
			return 1.0; //zadne zvetseni
		}
	}
	
	
	
	
	
	
	
	
	private void offerSave(Stage stage){
		FileChooser fileChooser = new FileChooser();
		final String IMG_EXTENSIONS = "Image (*.png, *.jpg, *.svg)";
		final String[] IMG_FILTERS = {"*.png", "*.jpg", "*.svg"}; 
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(IMG_EXTENSIONS, IMG_FILTERS));
		final String SER_EXTENSIONS = "Serialization (*.ser)";
		final String[] SER_FILTERS = { "*.ser" };
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(SER_EXTENSIONS, SER_FILTERS));
		fileChooser.setTitle("Save as");
		
		File selectedFile = fileChooser.showSaveDialog(stage);
		if (selectedFile != null){
			if (fileChooser.getSelectedExtensionFilter().getDescription().equals(SER_EXTENSIONS)){
				serializeGraph(selectedFile.getAbsolutePath());
			} else {
				ImagePrinter.save(selectedFile, (int)playground.getWidth(), (int)playground.getHeight(), graf);
			}
		}
	}
	
	private void serializeGraph(String filename){
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
			graf.prepareForSerialization();
			out.writeObject(graf);
		} catch(IOException e){
			e.printStackTrace();
			Err.showAlert("Could not serialize into this file. (Maybe it is being used by another application or you don't have sufficient rights to wite to it, etc.). Please try again, or choose a different file.");
		}	
	}
	
	private void deserializeGraph(String filename){
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))) {
			graf = (VisualGraph)in.readObject();
			playground.getChildren().clear();
			graf.finishDeserialization(playground);
		} catch(IOException e){
			e.printStackTrace();
			Err.showAlert("Could not read this file. (Maybe it is being used by another application or you don't have sufficient rights to wite to it, etc.). Please check that and try again.");
		} catch(ClassNotFoundException e){ //MEBBE handle the other exceptions
			e.printStackTrace();
			Err.showAlert("Fatal error");
			disposeOfGraph();
		}
	}
	
	private void disposeOfGraph(){
		playground.getChildren().clear();
		graf = null;	
	}
	
	private void showEdgeLabelsByChoice(){
		graf.showEdgeLabelsByChoice(menuShowEdgeLabels.isSelected(), menuShowShortEdgeLabels.isSelected(), menuShowFullEdgeLabels.isSelected());
	}
	private void showNodeLabelsByChoice(){
		graf.showNodeLabelsByChoice(menuShowNodeLabels.isSelected(), menuShowShortNodeLabels.isSelected(), menuShowFullNodeLabels.isSelected());
	}
	
	
	/**
	 * 
	 * @param stage - parent dialogu, kt. bude blokovat, dokud dialog nezmizi
	 */
	public void setListeners(Stage stage){
		

		
		sliderZoom.valueProperty().addListener((ov, old_val, new_val)-> {
				double mag = getMag(new_val.doubleValue()-old_val.doubleValue());
				graf.zoom(mag, new Coordinate(0.0, 0.0)); //TODO predelat
				
				/*  //for future debugging
					System.out.println(new_val.doubleValue()-old_val.doubleValue());
					System.out.println(mag);
				*/
                });
		
		menuFileOpen.setOnAction(new EventHandler<ActionEvent>(){
			@Override public void handle(ActionEvent e){				
				FileChooser fileChooser = new FileChooser();
				final String RDF_EXTENSIONS = "Rdf";
				final String[] RDF_FILTERS = { "*.ttl", "*.rdf", "*.nt", "*.jsonld", "*.rj", "*.trig", "*.nq" };
				/* TODO test each file extension
				 * 
				 * 
				 * turtle
				 * rdf/xml
				 * n-triples
				 * json-ld
				 * rdf/json
				 * trig
				 * n-quads
				 * */
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(RDF_EXTENSIONS, RDF_FILTERS));
				
				final String SCHE_EXTENSIONS = "Rdf schema";
				final String[] SCHE_FILTERS = { "*.rdfs", "*.owl" };
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(SCHE_EXTENSIONS, SCHE_FILTERS));
				
				final String SER_EXTENSIONS = "Serialization (*.ser)";
				final String[] SER_FILTERS = { "*.ser" };
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(SER_EXTENSIONS, SER_FILTERS));
				
				final String TITLE = "Open RDF File";
				fileChooser.setTitle(TITLE);
				File selectedFile = fileChooser.showOpenDialog(stage);
				
				if (selectedFile != null){
					//MEBBE offer to save previous work
					
					String selectedExt = fileChooser.getSelectedExtensionFilter().getDescription();
					String path = selectedFile.getAbsolutePath(); //MEBBE catch security exception
					
					if (selectedExt.equals(SER_EXTENSIONS)){
						deserializeGraph(path);
					} else {
						Path out = null;
						if (selectedExt.equals(SCHE_EXTENSIONS)){
							final String TEMP = "\\TEMP1238910.rdf";
							Path src = Paths.get(path);
							out = Paths.get(src.getParent().toString().concat(TEMP)); //MEBBE handle exception
							try{
								Files.copy(src, out, StandardCopyOption.COPY_ATTRIBUTES); //copy attributes neni potreba - je tam jen tak aby tam neco bylo
							} catch (IOException exc){ //MEBE lepe pocheckovat exceptions
								exc.printStackTrace();
								Err.showAlert("Could not access file. (Maybe it is being use by another program, or a file called TEMP1238910.rdf already exsts in this directory - which would also cause this error).");
							}
							path = out.toString();
						}
						
						playground.getChildren().clear();
						graf = new VisualGraph(playground, path);
						showEdgeLabelsByChoice(); //graf zde nesmi byt null (neni)
						showNodeLabelsByChoice();
						graf.scrambleRandom();
						
						if (selectedExt.equals(SER_EXTENSIONS)){
							try {
								Files.delete(out);
							} catch(IOException e2){
								e2.printStackTrace();
								Err.showAlert("Could not delete temporary file TEMP1238910.rdf. Please delete it manually later");
							}					
						}
					}
				}
			}
		});
		
		menuFileSaveAs.setOnAction(e->offerSave(stage));
		
		menuFileClose.setOnAction((ActionEvent e)->{
			disposeOfGraph();		
		});
		
		menuShowShortEdgeLabels.setOnAction((e)->{
			menuShowEdgeLabels.setSelected(false);
			menuShowFullEdgeLabels.setSelected(false);
			showEdgeLabelsByChoice();
		});
		menuShowShortNodeLabels.setOnAction((e)->{
			menuShowNodeLabels.setSelected(false);
			menuShowFullNodeLabels.setSelected(false);
			showNodeLabelsByChoice();
		});
		
		menuShowEdgeLabels.setOnAction((e)->{
			menuShowShortEdgeLabels.setSelected(false);
			menuShowFullEdgeLabels.setSelected(false);
			showEdgeLabelsByChoice();
		});
		menuShowNodeLabels.setOnAction((e)->{
			menuShowShortNodeLabels.setSelected(false);
			menuShowFullNodeLabels.setSelected(false);
			showNodeLabelsByChoice();
		});
		
		menuShowFullEdgeLabels.setOnAction((e)->{
			menuShowShortEdgeLabels.setSelected(false);
			menuShowEdgeLabels.setSelected(false);
			showEdgeLabelsByChoice();
		});
		menuShowFullNodeLabels.setOnAction((e)->{
			menuShowShortNodeLabels.setSelected(false);
			menuShowNodeLabels.setSelected(false);
			showNodeLabelsByChoice();
		});
		

		buttonReset.setOnAction(new EventHandler<ActionEvent>(){
			@Override public void handle(ActionEvent e){
				graf.scrambleRandom();
			}
		});
		
		buttonScrambleEggs.setOnAction(new EventHandler<ActionEvent>(){
			@Override public void handle(ActionEvent e){
				graf.scrambleForceDirected();
			}
		});
		
		
		zin.setOnAction((e)->zoom(1.2));
		zout.setOnAction((e)->zoom(1/1.2));
	}
	
	
	
	
	
}