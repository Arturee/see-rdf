package application;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;


import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
/**
 * Can print png, jpg and svg images of the currently shown graph.
 * @author Artur
 *
 */
public class ImagePrinter {
	
	public static void save(File file, int widthPixels, int heightPixels, VisualGraph g){
		String name = file.getName();
		String ext = name.replaceFirst(".*\\.", "").toUpperCase();
		if (ext.equals("PNG") || ext.equals("JPG")){
			savePNGJPG(file, ext, widthPixels, heightPixels, g);
		} else if (ext.equals("SVG")){
			saveSVG(file.getAbsolutePath(), g);
		} else {
			Err.showAlert("Try ending the name with a correct extension (.jpg, .png or .svg)");
		}
		
	}
	
	private static void savePNGJPG(File file, String pngOrJpg, int widthPixels, int heightPixels, VisualGraph g){
		BufferedImage bi = new BufferedImage(widthPixels, heightPixels, BufferedImage.TYPE_INT_RGB); //zde nesmi byt TYPE_INT_ARGB, protoze to invertuje barvy z nejakeho duvodu
		Graphics2D g2d = bi.createGraphics();
		
		//background color
		g2d.setColor(new Color(0xf2f2f2));
		g2d.fillRect(0, 0, widthPixels, heightPixels);
		
		g.paint(g2d);
		
		try {
			ImageIO.write(bi, pngOrJpg, file);
		} catch(IOException e){
			handleException(e);
		}
	}

	
	private static void saveSVG(String filepath, VisualGraph g){
		//POZOR vektorovy kanvas je nekonecny, ale cokoliv umistene na negativni pozice se orizne
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

	    // Create an instance of org.w3c.dom.Document.
	    final String SVG_NAMESPACE = "http://www.w3.org/2000/svg";
	    Document document = domImpl.createDocument(SVG_NAMESPACE, "svg", null);

	    // Create an instance of the SVG Generator.
	    SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

	    g.paint(svgGenerator);

	    // Finally, stream out SVG to the standard output using
	    // UTF-8 encoding.
	    final boolean USE_CSS = true; // we want to use CSS style attributes

		Path p = Paths.get(filepath);
		final Charset CHARSET = Charset.forName("UTF-8");
		try (BufferedWriter bw = Files.newBufferedWriter(p, CHARSET)) {
		    svgGenerator.stream(bw, USE_CSS);
		} catch (IOException e) {
			handleException(e);
		}
	}
	
	private static void handleException(IOException e){
		e.printStackTrace();
		Err.showAlert("Could not save the image (maybe it is open in a different program or you don't have sufficient rights to create/replace it), please try again.");
	}
	
}
