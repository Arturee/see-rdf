package application;



import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;



public class Seerdf extends Application {

	
	@Override
	public void start(Stage primaryStage) {
		try {
			/*
			 *	Na primary stage se pripichne scena a na ni guiRoot a ten je ovladany controllerem. 
			 *	Dale se na stage pripichuji Dialogy - proto je potreba ho predat funcki "mainController.setListeners(primaryStage);" 
			 */

			
			Parent guiRoot;
			MainFormController mainController;
			
			try {
				final String MAIN_FORM_DEFINITION = "MainForm.fxml";
				FXMLLoader loader = new FXMLLoader(Seerdf.class.getResource(MAIN_FORM_DEFINITION));
				/*
				 *	getresource() najde resource MainForm.fxml ve stejnem souboru jako je Main.class a vrati jeho abs path
				 *	loader nacte GUI z .fxml a vytvori instanci Controlleru a slinkuje ji s GUI
				 *	.fxml tedy musi byt nekde v /bin
				 */
				
				
				guiRoot = loader.load(); //loader preda GUI objekt
				mainController = (MainFormController)loader.getController(); //loader preda controller GUI objektu
			} catch (IOException e) {
				e.printStackTrace();
				Err.showAlert("Fatal error.");
				return;
			}
			
			
			mainController.setListeners(primaryStage);

			Scene scene = new Scene(guiRoot);
			primaryStage.setScene(scene);
			primaryStage.setTitle("See RDF");
			primaryStage.sizeToScene();
			primaryStage.show();
			
			
			ColorDialogController.create();

			//po skoneceni start() aplikace nyni ceka na guiEventy a krizkem se nakonec vypne
		} catch(NullPointerException e) {
			e.printStackTrace();
			Err.showAlert("Fatal error.");
			return;
		}
	}
	
	public static void main(String[] args) { //TODO toto je mozna zbytecne - pocheckovat
		launch(args);
	}
}
