package application;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.paint.Color;

public class ColorDialogController implements Initializable {
	
	
	@FXML private ChoiceBox<String> choiceBoxStyle;
	@FXML private ColorPicker colorPickerBorder;
	
	
	
	private static ColorDialogController colorController;
	private static Dialog<ButtonType> dialog;
	
	
	public static void create(){		
		try {
			final String COLOR_DIALOG_DEFINITION = "ColorDialog.fxml";
			URL url = Seerdf.class.getResource(COLOR_DIALOG_DEFINITION);
			FXMLLoader loader = new FXMLLoader(url);
	
			DialogPane pane = loader.load();
			
			colorController = (ColorDialogController)loader.getController();
			dialog = new Dialog<>();
			dialog.setDialogPane(pane);

			ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeCancel);
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
			Err.showAlert("Fatal error.");
		}
	}
	

	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//aby se dalo nacist GUI z fxml viz loader.load();
		choiceBoxStyle.setItems(FXCollections.observableArrayList("solid", "dashed", "dotted"));
		choiceBoxStyle.setValue("solid");
	}
	
	public static void show(VisualNode sourceNode){
		//colorController.choiceBoxStyle.setValue(sourceNode.lineStyle);
		//colorController.colorPickerBorder = new ColorPicker(Color.web(VisualNode.getWebColor(sourceNode.cborder)));
		//TODO nastavit prompt dle aktualniho stavu (kod vyse zatim nefunguje)
		
		Optional<ButtonType> res = dialog.showAndWait();
		if (res.isPresent()&& res.get().getButtonData() == ButtonData.OK_DONE){ //Okay-button or enter or space was pressed	
			String style = colorController.choiceBoxStyle.getSelectionModel().getSelectedItem();
			Color cborder = colorController.colorPickerBorder.getValue();
			sourceNode.setLineStyle(style);
			sourceNode.cborder = cborder;
			sourceNode.setStyle();
		}
	}
	
	
	
}
