package application;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;


public class CyclicArrow extends Arrow {
	private Circle edge;
	private Label nodeLabel;
	
	public CyclicArrow(String text, Pane parent, Label nodeLabel){
		super(text);
		this.nodeLabel = nodeLabel;
		final double RADIUS = 20.0;
		edge = new Circle();
		edge.setRadius(RADIUS);
		edge.setStroke(Color.web("#b3b3ff"));
		edge.setFill(null);
		setEdgeOnGui();
		parent.getChildren().addAll(label, edge, tip);
	}
	
	@Override
	public void paint(Graphics2D g){
		//paint edge
		g.setColor(new java.awt.Color(0xB3B3FF)); //blue hint
		double r = edge.getRadius();
		double d = r * 2;
		Coordinate center = new Coordinate(edge.getCenterX(), edge.getCenterY());
		center.x -= r;
		center.y -= r;
		Coordinate lu = center; center = null;
		
		Ellipse2D.Double circle = new Ellipse2D.Double(lu.x, lu.y, d, d);
		g.draw(circle);
		
		super.paint(g);
	}
	
	@Override
	public void refreshOnGui(Coordinate fromCenter, Coordinate toCenter, Label to){
		setEdgeOnGui();
	}
	

	
	
	private Coordinate getAnchor(){
		final double SUB = 5.0;
		Coordinate lu = new Coordinate(nodeLabel.getLayoutX(), nodeLabel.getLayoutY());
		lu.x += nodeLabel.getWidth();
		lu.x -= SUB;
		lu.y -= SUB;
		return lu;
	}
	
	private void setTip(){
		final double LOWERING_OFFSET = 2;
		final double NORMAL_LABEL_HEIGHT = 27.0;
		double height = nodeLabel.getHeight() != 0 ? nodeLabel.getHeight() : NORMAL_LABEL_HEIGHT; //workaround pro deserializaci, kde .height == 0.0
		Coordinate target = new Coordinate(nodeLabel.getLayoutX() + nodeLabel.getWidth(), nodeLabel.getLayoutY() + height/2 + LOWERING_OFFSET);
		final Vector DIRECTION = new Vector(1.13, -1).normalized();
		setArrowTip(target, DIRECTION);
	}
	
	private void setEdgeOnGui(){
		Coordinate anchor = getAnchor();
		edge.setCenterX(anchor.x);
		edge.setCenterY(anchor.y);
		
		double r = edge.getRadius();
		label.setLayoutX(anchor.x + r);
		label.setLayoutY(anchor.y - 2*r);
		
		setTip();
	}
}
