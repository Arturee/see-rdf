package application;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

public abstract class Err {
	
	private static Alert alert = new Alert(AlertType.ERROR, "nothing", ButtonType.OK);
	
	public static void showAlert(String text){
		alert.setContentText(text);
		alert.showAndWait();
	}


}
