package application;

import javafx.scene.paint.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class VisualNode implements Serializable {
	private static final long serialVersionUID = 1L; //ser
	
	private transient DraggableLabel guiRepresentation;
	private List<VisualEdgeDirected> outEdges;
	private List<VisualEdgeDirected> inEdges;
	private String uri;
	public transient Color cborder, cbackground; //cerna nefunguje ... Integer.getHexString() da 0 misto 000000
	private String lineStyle; //solid, dashed, dotted
	
	//ser
	private String serText;
	private double serX, serY;
	private String serCborder;
	
	
	//TODO safe setSyle method
	
	public VisualNode(){} //ser
	public VisualNode(String uri, Pane parent){
		this.uri = uri;

		inEdges = new ArrayList<>();
		outEdges = new ArrayList<>();
		
		cborder 	= Color.web("#FF8000");	//red
		cbackground = Color.web("#FFFFFF"); //white
		lineStyle	= "solid";
		guiRepresentation = getGUIRepresentation(uri, 0.0, 0.0);
		setStyle();
	}
	
	public void prepareForSerialization(){ //ser
		//pulls data form gui  (gui is not serializable)
		serText = guiRepresentation.getText();
		serX = guiRepresentation.getLayoutX();
		serY = guiRepresentation.getLayoutY();
		serCborder = ColorUtil.fxToWeb(cborder);
	}
	public void finishDeserialization(){ //ser
		guiRepresentation = getGUIRepresentation(serText, serX, serY);
		cborder = Color.web(serCborder);
		final String WHITE = "#FFFFFF";
		cbackground = Color.web(WHITE);
		setStyle();
	}
		
	public String getLineStyle(){
		return lineStyle;
	}
	public void setLineStyle(String s){
		if (s.equals("solid") || s.equals("dashed") || s.equals("dotted")){
			lineStyle = s;
		}
	}
	public void setStyle(){
		final String STYLE = "-fx-padding: 2px 10px 2px 10px;"
				+ "-fx-border-style: " + lineStyle
				+ "; -fx-border-width: 1px"
				+ "; -fx-border-color: " + ColorUtil.fxToWeb(cborder)
				+ "; -fx-background-color: " +  ColorUtil.fxToWeb(cbackground)
				+ ";";
		guiRepresentation.setStyle(STYLE);	
	}
	public void show(Pane parent){
		parent.getChildren().add(guiRepresentation);
	}
	public boolean isNeighbour(VisualNode n){
		for(Iterator<VisualEdgeDirected> it = outEdges.iterator(); it.hasNext(); ){
			VisualEdgeDirected e = it.next();
			if (e.getTarget() == n){
				return true;
			}
		}
		for(Iterator<VisualEdgeDirected> it = inEdges.iterator(); it.hasNext(); ){
			VisualEdgeDirected e = it.next();
			if (e.getSource() == n){
				return true;
			}
		}
		return false;
	}
	public void paint(Graphics2D g){
		int width = (int)guiRepresentation.getWidth();
		int height = (int)guiRepresentation.getHeight();
		int x = (int)guiRepresentation.getLayoutX();
		int y = (int)guiRepresentation.getLayoutY();
		String str = guiRepresentation.getText();
		
		final int RENDER_FONT_SIZE = 15;
		g.setFont(new Font(Font.DIALOG, Font.PLAIN, RENDER_FONT_SIZE));
		FontMetrics fm = g.getFontMetrics();
		Rectangle2D textBounds = fm.getStringBounds(str, g);
		double textWidth = textBounds.getWidth();
		double textPaddingLeft = (width - textWidth)/2;
		double textX = x + textPaddingLeft;
		
		final double Y_CORRECTION = -2.0;
		double textHeight = textBounds.getHeight();
		double textPaddingTop = (height - textHeight)/2;
		double textY = y + textHeight + textPaddingTop + Y_CORRECTION;
		 

		g.setColor(ColorUtil.fxToAwt(cbackground)); //white
		g.fillRect(x, y, width, height);
		
		g.setColor(ColorUtil.fxToAwt(cborder)); //red
		g.drawRect(x, y, width, height);
		
		final java.awt.Color BLACK = new java.awt.Color(0x000000); 
		g.setColor(BLACK);
		g.drawString(str, (int)textX, (int)textY); //lower left corner
	}
	public Coordinate getLeftUpper(){
		return new Coordinate(guiRepresentation.getLayoutX(), guiRepresentation.getLayoutY());
	}
	public void relocateCenter(Coordinate newCenter){
		double left = newCenter.x - guiRepresentation.getWidth()/2;
		double top = newCenter.y - guiRepresentation.getHeight()/2;
		relocate(new Coordinate(left, top));
	}
	public void relocate(Coordinate newUpperLeftCorner){
		guiRepresentation.relocate(newUpperLeftCorner.x, newUpperLeftCorner.y);
	}
	public void addEdgeOut(VisualEdgeDirected e){
		outEdges.add(e);
	}
	public void addEdgeIn(VisualEdgeDirected e){
		inEdges.add(e);
	}
	public Coordinate getCenter(){
		double x = guiRepresentation.getLayoutX() + guiRepresentation.getWidth()/2;
		double y = guiRepresentation.getLayoutY() + guiRepresentation.getHeight()/2;
		return new Coordinate(x, y);
	}
	public String getUri(){
		return uri;
	}
	public void showShortened(String shortened){
		this.guiRepresentation.setText(shortened);
	} 
	
	public void showFullUri(){
		this.guiRepresentation.setText(uri);
	}
	public Label getLabel(){ //MEBBE predelat aby nemusel vracet celou Labelu
		return guiRepresentation;
	}
	
	
	private DraggableLabel getGUIRepresentation(String text, double x, double y){
		DraggableLabel guiRepresentation = new DraggableLabel(this);
		guiRepresentation.setText(text);
		guiRepresentation.setLayoutX(x);
		guiRepresentation.setLayoutY(y);
		
		//set listeners for refreshing edges
		guiRepresentation.layoutXProperty().addListener(
			(ObservableValue<? extends Number> ov, Number old_val, Number new_val)->
			{ refreshEdges(); }
			);
		guiRepresentation.layoutYProperty().addListener(
			(ObservableValue<? extends Number> ov, Number old_val, Number new_val)->
			{ refreshEdges(); }
			);
		guiRepresentation.widthProperty().addListener(
			(change, oldVal, newVal)->
			{ refreshEdges(); }
			);
		return guiRepresentation;
	}
	private void refreshEdges(){
		for(VisualEdgeDirected e : outEdges){
			e.refresh();
		}
		for(VisualEdgeDirected e : inEdges){
			e.refresh();
		}
	}
	
	
	
	

	
	
	
}
