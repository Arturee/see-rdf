package application;


import java.awt.Graphics2D;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class StraightArrow extends Arrow {
	private Line edge;
	
	public StraightArrow(String text, Pane parent, Coordinate fromCenter, Coordinate toCenter, Label toLabel){
		super(text);
		edge = new Line();
		edge.setStroke(Color.web("#b3b3ff"));
		setFromTo(fromCenter, toCenter, toLabel);
		parent.getChildren().addAll(label, edge, tip);
	}

	@Override
	public void paint(Graphics2D g){
		//paint edge
		g.setColor(new java.awt.Color(0xB3B3FF)); //blue tint
		g.drawLine((int)edge.getStartX(), (int)edge.getStartY(), (int)edge.getEndX(), (int)edge.getEndY());
		
		super.paint(g);
	}
	
	@Override
	public void refreshOnGui(Coordinate fromCenter, Coordinate toCenter, Label to){
		setFromTo(fromCenter, toCenter, to);
	}
	
	public void setFromTo(Coordinate fromCenter, Coordinate toCenter, Label to){		

		edge.setStartX(fromCenter.x);
		edge.setStartY(fromCenter.y);
		edge.setEndX(toCenter.x);
		edge.setEndY(toCenter.y);
		
		//MEBBE jinak
		label.setLayoutX(fromCenter.x + (toCenter.x/2-fromCenter.x/2));
		label.setLayoutY(fromCenter.y + (toCenter.y/2-fromCenter.y/2));
				
		Vector dir = Vector.getVectorFromAToB(toCenter, fromCenter).normalized();
		Coordinate tipCoordinate = getArrowTipByTrialAndError(toCenter, dir, to);
		if (tipCoordinate!=null){
			setArrowTip(tipCoordinate, dir);
		} else {
			setArrowTip(toCenter, dir);
		}
	}
	

	private Coordinate getArrowTipByTrialAndError(Coordinate center, Vector direction, Label to){
		final Double NEGLIGIBLE = 0.1;
		if (to.getWidth() < NEGLIGIBLE || to.getHeight() < NEGLIGIBLE) {
			return null; //label bounds are not set yet
		}
		Coordinate lu = new Coordinate(to.getLayoutX(), to.getLayoutY());
		Coordinate rl = new Coordinate(to.getLayoutX()+to.getWidth(), to.getLayoutY()+to.getHeight());
		Coordinate tip = center;
		final int START_PIXELS = 30;
		final double DIV_FACTOR = 2;
		final int ROUNDS = 2; //algo ends at 1 pixel accuracy
		direction = Vector.scalarMultiply(direction, START_PIXELS);
		if (direction.magnitude() < 16){ 
			return null;  //direction vector is (0.0, 0.0)
		}
		
		for (int i =0; i<ROUNDS; i++){	
			while (isInRectangle(tip, lu, rl)){
				tip.add(direction);
			}
			direction = Vector.scalarMultiply(direction, -1/DIV_FACTOR);
			while(!isInRectangle(tip, lu, rl)){
				tip.add(direction);
			}
			direction = Vector.scalarMultiply(direction, -1/DIV_FACTOR);
		}
		return tip;
	}
	
	private boolean isInRectangle(Coordinate what, Coordinate lu, Coordinate rl){ //lu ma nejnizsi hodnoty, rl nejvyssi
		return what.x >= lu.x && what.x <= rl.x &&what.y >= lu.y && what.y <= rl.y;
	}
	
	
}
