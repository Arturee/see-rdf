package application;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Shortcut {
	public static List<Shortcut> list;
	static {
		list = loadShortcuts();
	}
	
	public String abbr;
	public String uri;
	
	public Shortcut(String abbr, String uri){
		this.abbr = abbr;
		this.uri = uri;
	}
	public boolean fits(String dataUri){
		return dataUri.matches("^" + uri + ".*$") || dataUri.matches("^.*\\^\\^" + uri + ".*$");
	}
	public String abbreviate(String fullUri){ //MEBBE zlepsit
		String breif = fullUri.replaceFirst(uri, abbr+":");
		if (breif.equals(fullUri)){
			breif = fullUri.replaceFirst("\\^\\^" + uri, abbr+":");
		}
		return breif; 
	}
	public String abb10(String fullUri){
		final int LETTERS = 10;
		String abb = abbreviate(fullUri);
		String[] par = abb.split(":");
		String word = par[1];
		int index = word.length()-LETTERS;
		if (index > 0){
			String ten = word.substring(index);
			return par[0] + "-" + ten;
		} else {
			return abb;
		}
	}
	
	public static ObservableList<String> getShortcutsForGUI(){
		ObservableList<String> os = FXCollections.observableArrayList();
		for (Shortcut s : list){
			os.add(s.toString());
		}
		return os;
	}
	
	
	@Override
	public String toString(){
		final int ABBR_COLUMN_WIDTH = 10;
		StringBuilder sb = new StringBuilder(abbr);
		sb.append(":");
		for(int i=abbr.length();i<ABBR_COLUMN_WIDTH;i++){
			sb.append(" ");
		}
		sb.append(uri);
		return sb.toString();
	}
	
	
	public static List<Shortcut> loadShortcuts(){ //POZOR jeden shortcut nesmi byt prefixem jineho
		final String SHORTCUT_FILE = "config//shortcuts.txt";
		final Charset CHARSET = Charset.forName("UTF-8");
		
		ArrayList<Shortcut> list = new ArrayList<>();
		
		Path p = Paths.get(SHORTCUT_FILE);
		try (BufferedReader br = Files.newBufferedReader(p, CHARSET)) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] par = line.split("\\s+");
				if (par.length != 2){
					Err.showAlert("Shortcut file is invalid. Problematic line = " + line + ". Please correct that and try again.");
					return null;
				}
				String abbr = par[0];
				String uri = par[1]; //MEBBE check if actual uri?
				if (!abbr.matches("[a-zA-Z0-9]+")){
					Err.showAlert("Shortcut file is invalid. Shortcuts can contain only letters and numbers. Problem = " + abbr + ". Please correct that and try again.");
					return null;
				}
				Shortcut sh = new Shortcut(abbr, uri);
				for (Iterator<Shortcut> it = list.iterator(); it.hasNext();){
					String uri2 = it.next().uri;
					if (uri.startsWith(uri2)||uri2.startsWith(uri)){
						Err.showAlert("Shortcut file is invalid. One Uri cannot be a prefix of another. Please correct that and try again.");
						return null;
					}
				}
				list.add(sh);
			}
		} catch (IOException e) {
			e.printStackTrace();
			Err.showAlert("File config/shortcuts.txt could not be read (maybe it is open by another application or you dont have sufficient rights to read it), please check that and try again.");
			return null;
		}
		return list;
	}
	
	
}
