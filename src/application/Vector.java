package application;


public class Vector {
	public double x,y;
	
	public Vector(){
		this(0.0, 0.0);
	}
	public Vector(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public Vector normalized(){
		double mag = magnitude();
		final double NEGLIGIBLE = 0.001;
		if (mag< NEGLIGIBLE){
			return new Vector(0.0, 0.0); //zabrani NaN
		} else {
			return new Vector(x/mag, y/mag);
		}
	}
	public double magnitude(){
		return Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
	}
	public double getRealLifeDistance(){
		final int PIXELS_PER_UNIT = 100;
		return magnitude() / PIXELS_PER_UNIT;
	}
	public void add(Vector v){
		this.x += v.x;
		this.y += v.y;
	}
	public void scalarMultiply(double scalar){
		this.x *= scalar;
		this.y *= scalar;
	}

	public static Vector scalarMultiply(Vector v, double d){
		return new Vector(v.x*d, v.y*d);
	}
	public static Vector getVectorFromAToB(Coordinate a, Coordinate b){
		return new Vector(b.x-a.x, b.y-a.y);
	}
	

}
