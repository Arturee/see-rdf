package application;



import java.awt.Font;
import java.awt.Graphics2D;

import javafx.collections.ObservableList;
import javafx.scene.control.Label;

import javafx.scene.paint.Color;

import javafx.scene.shape.Polygon;

public abstract class Arrow {
	protected Label label;
	protected Polygon tip;
	
	public Arrow(String text){
		label = new Label();
		label.setText(text);
		
		tip = new Polygon();
		tip.setStroke(Color.web("#b3b3ff"));
		tip.setFill(Color.web("#b3b3ff"));
	}
	
	public abstract void refreshOnGui(Coordinate fromCenter, Coordinate toCenter, Label to);
	
	public String getText(){
		return label.getText();
	}
	public void setText(String text){
		label.setText(text);
	}
	
	
	public void paint(Graphics2D g){
		final int RENDER_FONT_SIZE = 15; //workaround, protoze pouziti font size z GUI skonci moc maly pismem
		
		//paint label
		final double Y_CORRECTION = -4.0; //workaround proti paddingum
		g.setColor(new java.awt.Color(0x000000)); //black
		g.setFont(new Font(Font.DIALOG, Font.PLAIN, RENDER_FONT_SIZE));
		Coordinate ll = new Coordinate(label.getLayoutX(), label.getLayoutY()+label.getHeight()+Y_CORRECTION);
		g.drawString(label.getText(), (int)ll.x, (int)ll.y);
		
		//paint arrow tip
		final int POINTS_IN_ARROW = 4;
		int[] xpoints = new int[POINTS_IN_ARROW];
		int[] ypoints = new int[POINTS_IN_ARROW];
		
		ObservableList<Double> dbls = tip.getPoints();
		for (int i =0; i< dbls.size(); i++){
			if (i % 2 == 0){
				xpoints[i/2] = dbls.get(i).intValue();
			} else {
				ypoints[i/2] = dbls.get(i).intValue();
			}
		}
		java.awt.Polygon p = new java.awt.Polygon(xpoints, ypoints, POINTS_IN_ARROW);
		
		g.setColor(new java.awt.Color(0xB3B3FF)); //blue tint
		g.drawPolygon(p);
		g.fillPolygon(p);	
	}
		
	
	protected void setArrowTip(Coordinate target, Vector backDirection){
		tip.getPoints().clear();
		//tip of the tip
		tipAddPoint(target);
		
		final double LENGTH = 20.0;
		final double NARROWING_FACTOR = 0.2;
		final double INDENT_PRECENTAGE = 0.2;
		
		backDirection.scalarMultiply(LENGTH);
		
		Coordinate indent = new Coordinate(target);
		indent.add(Vector.scalarMultiply(backDirection, 1-INDENT_PRECENTAGE)); //indent
		
		target.add(backDirection);//base
		
		//get perpendicular vector and shorten it
		double y = backDirection.y * -1;
		backDirection.y = backDirection.x; 
		backDirection.x = y;
		backDirection.scalarMultiply(NARROWING_FACTOR);
		
		target.add(backDirection); //bottom left or right corner
		tipAddPoint(target);
		
		tipAddPoint(indent);
		
		backDirection.scalarMultiply(-1);
		target.add(backDirection);
		target.add(backDirection); //the ohter bootom corner
		tipAddPoint(target);
	}
	
	private void tipAddPoint(Coordinate c){
		tip.getPoints().add(c.x);
		tip.getPoints().add(c.y);
	}

	
	

	
}