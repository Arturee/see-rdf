package application;


import java.awt.Graphics2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import javafx.scene.layout.Pane;


public class VisualGraph implements Serializable {
	private static final long serialVersionUID = 1L; //ser

	private transient Pane view;
	
	private Collection<VisualEdgeDirected> edges; //TODO pozor deserialize vytvori hashmap misto list, protoze pouzivam collection - MEBBE predelat
	private Collection<VisualNode> nodes;
	
	private double serWidth, serHeight;
	
	
	
	public VisualGraph(){} //ser
	public VisualGraph(Pane view, String rdfFileName){
		this.view = view;
		edges = new HashSet<>();
		nodes = new HashSet<>();
		RdfController.loadRdfIntoCollections(rdfFileName, nodes, edges, view);
		showNodes(); //proc takto? - toto je workaround na z-index (aby edge neprekryvali nody - aby nody byly vepredu)
	}
	
	public void prepareForSerialization(){ //ser
		serWidth = view.getPrefWidth();
		serHeight = view.getPrefHeight();
		
		for (VisualNode n : nodes){
			n.prepareForSerialization();
		}
		for (VisualEdgeDirected e : edges){
			e.prepareForSerialization();
		}
	}
	public void finishDeserialization(Pane view){ //ser
		this.view = view;
		view.setPrefWidth(serWidth);
		view.setPrefHeight(serHeight);
		
		for (VisualNode n : nodes){
			n.finishDeserialization();
		}
		for (VisualEdgeDirected e : edges){
			e.finishDeserialization(view);
		}
		for (VisualNode n : nodes){
			n.show(view);
		}
	}
	
	
	
	
	private void showNodes(){
		for(VisualNode n : nodes){
			n.show(view);
		}
	}
		
	public void zoom(double mag, Coordinate center){
		final double MAX_MAG = 5.0;
		final double NEGLIGIBLE = 0.01;
		if (mag < NEGLIGIBLE || mag > MAX_MAG){
			return;
		}
		
		Coordinate worstOutlier = new Coordinate(0.0, 0.0); //ze vsech nodu nejhorsi x a nejhorsi y (nejvice zaporna)
		List<WrapVNodeCoordinate> newPositions = new ArrayList<>();
		
		for (VisualNode n : nodes){
			Coordinate lu = n.getLeftUpper();
			Vector v = Vector.getVectorFromAToB(center, lu);
			v.scalarMultiply(mag);
			Coordinate destination = new Coordinate(center);
			destination.add(v); //destination
			newPositions.add(new WrapVNodeCoordinate(n, destination));
			
			if (v.x < worstOutlier.x){
				worstOutlier.x = v.x;
			}
			if (v.y < worstOutlier.y){
				worstOutlier.y = v.y;
			}
		}
		//correction = posunout vse, tak aby worst outlyier byl na [0,0]
		Vector correction = new Vector(-worstOutlier.x, -worstOutlier.y);
		for (WrapVNodeCoordinate w : newPositions){
			w.c.add(correction);
			w.n.relocate(w.c);
		}
		
		view.setPrefSize(view.getPrefWidth()*mag, view.getPrefHeight()*mag); //TODO pohlidat MAx a Min
	}
	
	
	//TODO udelat predka pro node a edge a hezceji preprogramovat.
	public void showNodeLabelsByChoice(boolean defaultTen, boolean tiny, boolean full){
		if (defaultTen) {
			showShortNodeLabels(true);
		} else if (tiny) {
			showShortNodeLabels(false);
		} else if (full) {
			showFullNodeLabels();
		} else {
			hideNodeLabels();
		}
	}
	
	public void showEdgeLabelsByChoice(boolean defaultTen, boolean tiny, boolean full){
		if (defaultTen) {
			showShortEdgeLabels(true);
		} else if (tiny) {
			showShortEdgeLabels(false);
		} else if (full) {
			showFullEdgeLabels();
		} else {
			hideEdgeLabels();
		}
	}
	
	private void showShortNodeLabels(boolean ten){
		for (VisualNode n : nodes){
			String uri = n.getUri(); //can actually be just a value
			String shortened = null;
			for (Shortcut s : Shortcut.list){
				if (s.fits(uri)){
					if (ten){
						shortened = s.abb10(uri);
					} else {
						shortened = s.abbreviate(uri);
					}
					n.showShortened(shortened);
					break;
				}
			}
			if (shortened == null){ //na node nelze aplikovat shortcut
				if (ten){
					n.showShortened(toTen(uri));
				} else {
					n.showFullUri();
				}
			}
		}
	}
	
	private void showShortEdgeLabels(boolean ten){
		for (VisualEdgeDirected e : edges){
			String uri = e.getUri();
			String shortened = null;
			for (Shortcut s : Shortcut.list){
				if (s.fits(uri)){
					if (ten){
						shortened = s.abb10(uri);
					} else {
						shortened = s.abbreviate(uri);
					}
					e.showShortened(shortened);
					break;
				}
			}
			
			if (shortened == null){ //na edge nelze aplikovat shortcut
				if (ten){
					e.showShortened(toTen(uri));
				} else {
					e.showFullUri();
				}
			}
		}
	}
	
	private String toTen(String orig){
		int i = orig.length() - 10;
		if (i<1){
			return orig;
		} else {
			return "..."  + orig.substring(i); 
		}
	}
	
	private void showFullNodeLabels(){
		for (VisualNode n : nodes){
			n.showFullUri();
		}
	}
	private void showFullEdgeLabels(){
		for (VisualEdgeDirected e : edges){
			e.showFullUri();
		}
	}
	
	private void hideNodeLabels(){
		for (VisualNode n : nodes){
			n.showShortened("");
		}
	}
	private void hideEdgeLabels(){
		for (VisualEdgeDirected e : edges){
			e.showShortened("");
		}
	} 
	

	public void scrambleRandom(){
		NodePositioning.scrambleRandom(nodes, (int)view.getWidth(), (int)view.getHeight());
	}
	public void scrambleForceDirected(){
		NodePositioning.scrambleForceDirectedSpringElectric(nodes);
	}
	
	
	public void paint(Graphics2D g){
		for(Iterator<VisualEdgeDirected> it = edges.iterator(); it.hasNext();){
			it.next().paint(g);
		}
		for(Iterator<VisualNode> it = nodes.iterator(); it.hasNext();){
			it.next().paint(g);
		}
	}
	
	
	
	
	
	
}
