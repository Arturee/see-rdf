package application;

import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

public abstract class NodePositioning {
	
	//O(n)
	public static void scrambleRandom(Collection<VisualNode> nodes, int width, int height){
		Random rand = new Random();
		for (Iterator<VisualNode> it = nodes.iterator(); it.hasNext(); ){
			int rx = rand.nextInt(width);
			int ry = rand.nextInt(height);
			it.next().relocateCenter(new Coordinate(rx, ry));
		}

	}
	
	//O(n^2)
	public static void scrambleForceDirectedSpringElectric(Collection<VisualNode> nodes){
		//works the same for an undirected graph
		
		final int CYCLES = 100;
		for (int i =0; i<CYCLES; i++){
			scrambleForceDirectedSPlingElectrincOneStep(nodes);			
		}
	}
	
	private static void scrambleForceDirectedSPlingElectrincOneStep(Collection<VisualNode> nodes){
		for(Iterator<VisualNode> it = nodes.iterator(); it.hasNext(); ){
			VisualNode node = it.next();
			Vector forceVector = new Vector(0.0, 0.0);
			
			for(Iterator<VisualNode> it2 = nodes.iterator(); it2.hasNext(); ){
				VisualNode node2 = it2.next();
				if (node==node2){
					continue;
				} else if (node.isNeighbour(node2)){
					forceVector.add(calculateAttractiveSpringForceVector(node, node2));
					forceVector.add(calculateRepulsiveElectricForceVector(node, node2));
				} else {
					forceVector.add(calculateRepulsiveElectricForceVector(node, node2));
				}
			}

			final double DAMPING_FACTOR = 10; //dulezite! (recommended je 0.1, ale to neni vubec tak dobre)
			forceVector = Vector.scalarMultiply(forceVector, DAMPING_FACTOR);
			
			Coordinate position = node.getCenter();
			position.add(forceVector);
			node.relocateCenter(position);					
		}
	}
	
	
	private static Vector calculateAttractiveSpringForceVector(VisualNode pulled, VisualNode pulling){
		Vector diff = Vector.getVectorFromAToB(pulled.getCenter(), pulling.getCenter());
		double distance = diff.getRealLifeDistance();
		Vector directionVector = diff.normalized();
		
		final int STRENGTHENING_FACTOR = 2;
		final int SHORTENING_FACTOR = 1;
		double force = STRENGTHENING_FACTOR * Math.log10(distance/SHORTENING_FACTOR);

		return Vector.scalarMultiply(directionVector, force);
	}
	
	private static Vector calculateRepulsiveElectricForceVector(VisualNode repulsed, VisualNode repulsor){
		Vector diff = Vector.getVectorFromAToB(repulsor.getCenter(), repulsed.getCenter());
		double distance = diff.getRealLifeDistance();
		Vector directionVector = diff.normalized();
		
		final int STRENGTHENING_FACTOR = 1;
		double force = STRENGTHENING_FACTOR / Math.pow(distance, 2);

		return Vector.scalarMultiply(directionVector, force);
	}
		
}
